# zone_mu #

This project creates chart of the nuclide diagrams that show chemical potential offsets in nuclear reaction
network calculations.

### Steps to install ###

First, clone the repository by typing 

**git clone https://bitbucket.org/mbradle/zone_mu.git**

Next, ensure that wn_user is installed.  If you have not alreadly done so, type

**git clone https://bitbucket.org/mbradle/wn_user.git**

Change into the *zone_mu* directory and create the project by typing

**cd zone_mu**

**./project_make**

## Steps to create the diagrams ##

To get started, retrieve an example xml file by typing

**curl -o example.xml -J -L https://osf.io/xguz2/download**

Create a directory, preferably called *output*, where the output [graphviz](https://graphviz.org) files will be stored:

**mkdir output**

Run the *zone_mu* code on a previously created *single_zone_network* output xml file.  As an initial example, use the example input file *example.xml* read over the web.  Thus, type

**./zone_mu --libnucnet_xml example.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 3 and z <= 10 and a - z >= 4 and a - z <= 12]" --max_node_shape tripleoctagon**

This example execution will read the data from the file *example_data/out.xml*
(read over the web) and
create the chemical potential offset diagrams from those data.  It will create a subset of the full network
with atomic number *Z* in the range 3 <= Z <= 10 and neutron number *N = A - Z*, where *A* is the mass number,
also in the range 4 <= N <= 12.  In each diagram, the species with the maximum abundance within the scope of
the diagram will have the shape of a triple-outlined octagon
(see the [graphviz documentation](https://graphviz.org/documentation/) for the
[possible shapes](https://graphviz.org/doc/info/shapes.html)).  The output from the execution will be a number
[graphviz](https://graphviz.org) files (labeled *out_x.dot*, where *x* is the sequential label  in the directory *output*.

To run on your own *single_zone_network* output xml file called, say,
*my_out.xml*, type, for example

**./zone_mu --libnucnet_xml my_out.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 3 and z <= 10 and a - z >= 4 and a - z <= 12]" --max_node_shape tripleoctagon**

Use other options, as desired.  To see the other options available in running the *zone_mu* code, type

**./zone_mu --help**

and

**./zone_mu --prog all**

## Steps to create pdf files ##

Once you have created the appropriate *dot* files, you can convert them into pdfs.  To do so, follow these [steps](https://osf.io/36apc/wiki/home/).

## Steps to create a color bar diagram ##

The colors in the output pdfs indicate the chemical potential offset from the maximum species abundance within the
scope of the diagram.  To get numerical values corresponding to the colors, you can create a color bar.  To do so,
type

**python color_bar.py > my_color_bar.dot**

Use the options *--mu_min* and *--mu_max* to set the maximum and/or minimum chemical potential offset corresponding
to your chemical potential offset diagrams.  Now create a pdf from your graphviz file by typing

**dot2tex --crop my_color_bar.dot > my_color_bar.tex**

**pdflatex my_color_bar**

The output *my_color_bar.pdf* will give your color bar.  The numbers inside each box indicates the chemical
potential offset (divided by *kT*).

## Black and White Versions ##

It is possible to create a greyscale version of the figures.  This is done with the
*--b/w true* optional call.  For the run with *example.xml*, type

**./zone_mu --libnucnet_xml example.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 3 and z <= 10 and a - z >= 4 and a - z <= 12]" --max_node_shape tripleoctagon --b/w true**

To get the greyscale colorbar, type

**python bw_bar.py > my_bw_bar.dot**

As with the color bar, use the options *--mu_min* and *--mu_max* to set the maximum and/or minimum chemical potential offset corresponding
to your chemical potential offset diagrams.  Now create a pdf from your graphviz file by typing

**dot2tex --crop my_bw_bar.dot > my_bw_bar.tex**

**pdflatex my_bw_bar**

The output is *my_bw_bar.pdf*.  You can check the help for the codes to see the other options available.

## Steps to animate the diagrams ##

It is possible to combine the pdf files previously created into an animation.
To do so, follow these [steps](https://osf.io/mvwu2/wiki/home/)

