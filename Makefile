#///////////////////////////////////////////////////////////////////////////////
#  Copyright (c) 2017 Clemson University.
# 
#  This file was originally written by Bradley S. Meyer.
# 
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this software; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#  USA
# 
#/////////////////////////////////////////////////////////////////////////////*/

#///////////////////////////////////////////////////////////////////////////////
#//!
#//! \file Makefile
#//! \brief A makefile to generate code for the zone_mu project.
#//!
#///////////////////////////////////////////////////////////////////////////////

ifndef NUCNET_TARGET
NUCNET_TARGET = ../nucnet-tools-code
endif

ifndef WN_USER_TARGET
WN_USER_TARGET = ../wn_user
endif

#///////////////////////////////////////////////////////////////////////////////
# Here are lines to be edited, if desired.
#///////////////////////////////////////////////////////////////////////////////

SVNURL = http://svn.code.sf.net/p/nucnet-tools/code/trunk

BUILD_DIR = $(WN_USER_TARGET)/build
VENDORDIR = $(BUILD_DIR)/vendor
OBJDIR = $(BUILD_DIR)/obj

NNT_DIR = $(NUCNET_TARGET)/nnt
USER_DIR = $(NUCNET_TARGET)/user

#///////////////////////////////////////////////////////////////////////////////
# End of lines to be edited.
#///////////////////////////////////////////////////////////////////////////////

#===============================================================================
# Svn.
#===============================================================================

SVN_CHECKOUT := $(shell if [ ! -d $(NUCNET_TARGET) ]; then svn co $(SVNURL) $(NUCNET_TARGET); else svn update $(NUCNET_TARGET); fi )

#===============================================================================
# Includes.
#===============================================================================

include $(BUILD_DIR)/Makefile
include $(BUILD_DIR)/Makefile.sparse
include $(USER_DIR)/Makefile.inc

VPATH = $(BUILD_DIR):$(NNT_DIR):$(USER_DIR):$(WN_USER_TARGET)

#===============================================================================
# Add local directory to includes.
#===============================================================================

CFLAGS += -I ./ -I $(WN_USER_TARGET)

#===============================================================================
# Debugging, if desired.
#===============================================================================

ifdef DEBUG
  CFLAGS += -DDEBUG
  FFLAGS += -DDEBUG
endif

#===============================================================================
# my_user.
#===============================================================================

ifdef WN_USER
  CFLAGS += -DWN_USER
endif

ifdef HYDRO_EVOLVE
  CFLAGS += -DHYDRO_EVOLVE
endif

#===============================================================================
# Objects.
#===============================================================================

ZONE_MU_OBJ = $(WN_OBJ)	\
               $(WN_USER_OBJ)	\
               $(NNT_OBJ)	\
               $(SOLVE_OBJ)	\
               $(USER_OBJ)	\
               $(OPTIONS_OBJ)	\

ZONE_MU_DEP = $(ZONE_MU_OBJ)

#===============================================================================
# Executables.
#===============================================================================

ZONE_MU_EXEC = zone_mu

$(ZONE_MU_EXEC): $(ZONE_MU_DEP)
	$(HC) -c -o $(OBJDIR)/$@.o $@.cpp
	$(HC) $(ZONE_MU_OBJ) -o $(BINDIR)/$@ $(OBJDIR)/$@.o $(FLIBS) $(CLIBS)

.PHONY all_zone_mu: $(ZONE_MU_EXEC)

#===============================================================================
# Clean up. 
#===============================================================================

.PHONY: clean_zone_mu cleanall_zone_mu

clean_zone_mu:
	rm -f $(ZONE_MU_OBJ)

cleanall_zone_mu: clean_zone_mu
	rm -f $(BINDIR)/$(ZONE_MU_EXEC) $(BINDIR)/$(ZONE_MU_EXEC).exe

clean_nucnet:
	rm -fr $(NUCNET_TARGET)
