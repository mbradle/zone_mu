import numpy as np
import wnutils.xml as wx 
import argparse

parser = argparse.ArgumentParser(
            description='Create chemical potential offset color bar.',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--mu_min', metavar='mu_min', default=-750, type=float,
                    help='Minimum chemical potential offset')
parser.add_argument('--mu_max', metavar='mu_max', default=500, type=float,
                    help='Maximum chemical potential offset')

args = parser.parse_args()

x = np.array([-767, -646, -511, -382, -255, -127, 0, 127, 255, 382, 511])

c = ["#00FF00",  "#80FF00", "#FFFF00", "#FF8000", "#FF0000", "#FF0080", "#FF00FF", "#8000FF", "#0000FF", "#0080FF", "#00FFFF" ]

for i in range(len(x)):
    if x[i] >= 0:
        x[i] *= args.mu_max / 511
    else:
        x[i] *= -args.mu_min / 767

print("digraph G {")
print("node [shape=box]")

for i in range(len(x)):
    print(i, "[texlbl=\"\huge{", x[i], "}\" style=filled, fillcolor=\"", c[i], "\"];")

print("}")
