import numpy as np
import wnutils.xml as wx 
import argparse

parser = argparse.ArgumentParser(
            description='Create chemical potential offset b/w bar.',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--mu_min', metavar='mu_min', default=-750, type=float,
                    help='Minimum chemical potential offset')
parser.add_argument('--mu_max', metavar='mu_max', default=500, type=float,
                    help='Maximum chemical potential offset')
parser.add_argument('--bw_shift', metavar='bw_shift', default=100, type=float,
                    help='Offset at which to shift from black to white')

args = parser.parse_args()

x = np.linspace(0, 240, 11)

c = ["#000000", "#18181818", "#303030", "#484848", "#606060", "#787878", "#909090", "#a8a8a8", "#c0c0c0", "#d8d8d8", "#dcdcdc"]

v = []
for i in range(len(x)):
    v.append(args.mu_min + (x[i]/240.) * (args.mu_max - args.mu_min))

print("digraph G {")
print("node [shape=box]")

for i in range(len(x)):
    s_color = 'black'
    if v[i] <= args.bw_shift:
        s_color = 'white'
    print(i, "[texlbl=\"\huge{", int(v[i]), "}\" style=filled, fillcolor=\"", c[i], "\",  lblstyle=\"", s_color, "\"];")

print("}")
